﻿// See https://aka.ms/new-console-template for more information
using StudentApp;
using System.Collections;
//its a non generic collection
Console.WriteLine("sorted list");
SortedList sortedList = new SortedList();
sortedList.Add(101, new Student { Id=101,Name="tp",Age=22 });
sortedList.Add(90, new Student { Id = 90, Name = "ap", Age = 23 });
//by default its in ascendng order
foreach(DictionaryEntry item in sortedList)
{
    Console.WriteLine(item.Value);
}
//How to do descending order ..?
﻿// See https://aka.ms/new-console-template for more information
using System.Collections;

Console.WriteLine("demo::stack");
Stack logs = new Stack();
logs.Push("request come at 10am");
logs.Push("database collectoin");
logs.Push("logged in");
foreach(string log in logs)
{
    Console.WriteLine(log);
}
Console.WriteLine("Latest Log Message peek::-------");
Console.WriteLine(logs.Peek());
Console.WriteLine("--------------");
Console.WriteLine($"count::{logs.Count}");
foreach (string log in logs)
{
    Console.WriteLine(log);
}
Console.WriteLine("logs popped----------");
Console.WriteLine(logs.Pop());
Console.WriteLine("--------------");
Console.WriteLine($"count::{logs.Count}");
foreach (string log in logs)
{
    Console.WriteLine(log);
}
﻿// See https://aka.ms/new-console-template for more information

//WAP TO STORE COURSE CODES AND COURSE NAME IN A DICTIONARY
Console.WriteLine("Demo::Dictionary");
//Create Dictionary in C#
Dictionary<int,string> courses=new Dictionary<int,string>();
//Add Items to the dictionary
courses.Add(101, "programs in C#");
courses.Add(102, "angular");
courses.Add(103, "webdev");
foreach(KeyValuePair<int,string> course in courses)
{
    Console.WriteLine($"key::{course.Key}\t values::{course.Value}");
}
Console.WriteLine($"Course Key Available::{courses.ContainsKey(101)}");
//Remove in Dictionary
courses.Remove(101);
foreach (KeyValuePair<int, string> course in courses)
{
    Console.WriteLine($"key::{course.Key}\t values::{course.Value}");
}
Console.WriteLine($"Course Key Available::{courses.ContainsKey(101)}");
//Update in Dictionary
courses[102] = "ReactsJS";
foreach (KeyValuePair<int, string> course in courses)
{
    Console.WriteLine($"key::{course.Key}\t values::{course.Value}");
}
Console.WriteLine($"Course Key Available::{courses.ContainsKey(101)}");

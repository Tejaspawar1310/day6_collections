﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBackMethod
{
    internal class Class1
    {
        //delegate declaraion
        public delegate void Callback(int i);

        public void LongRunningProcess(Callback obj)
        {
            for(int i = 0; i < 100; i++)
            {
                obj(i);
            }
        }
    }
}

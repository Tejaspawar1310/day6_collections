﻿// See https://aka.ms/new-console-template for more information
using System.Collections;
Console.WriteLine("Arrya List in C#");
//creating array list
ArrayList arrayList = new ArrayList();
arrayList.Add(101);
Console.WriteLine($"count::{arrayList.Count}");
Console.WriteLine($"capacity::{arrayList.Capacity}");
arrayList.Add("tpp");
arrayList.Add(true);
foreach(var item in arrayList)
{
    Console.WriteLine(item);
}
//Get Individual Element from ArrayList
string secondElement = (string)arrayList[1];
Console.WriteLine(secondElement);

//Add values to array list
Console.WriteLine("Adding values to array list");
arrayList.Insert(1, 105);
arrayList.Insert(2, "mumbai");
//Display
foreach (var item in arrayList)
{
    Console.WriteLine(item);
}
//Insert range of values
Console.WriteLine("insert range of values");
ArrayList arrayList1=new ArrayList() { "area name","area two",1101};
arrayList.InsertRange(0,arrayList1);
foreach (var item in arrayList)
{
    Console.WriteLine(item);
}
//Remove ELEMENT
arrayList.Remove("mumbai");
arrayList.RemoveAt(1);

Console.WriteLine("item after removed");
foreach (var item in arrayList)
{
    Console.WriteLine(item);
}
//Remove Range
Console.WriteLine("after remove range");
arrayList.RemoveRange(0, 3);
foreach (var item in arrayList)
{
    Console.WriteLine(item);
}


﻿// See https://aka.ms/new-console-template for more information
using System.Collections;

Console.WriteLine("Demo::Queue");

Queue tickets = new Queue();
tickets.Enqueue("ticket1");
tickets.Enqueue("ticket2");
foreach(var tick in tickets)
{
    Console.WriteLine(tick);
}
Console.WriteLine($"peek::{tickets.Peek()}");
Console.WriteLine($"Count::{tickets.Count}");
tickets.Dequeue();
Console.WriteLine($"Count::{tickets.Count}");
foreach (var tick in tickets)
{
    Console.WriteLine(tick);
}
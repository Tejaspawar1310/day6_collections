﻿// See https://aka.ms/new-console-template for more information
//1.delegate declaration
//2.Instantiate A delegate
//3.Invoke Delegate

namespace demo_delegate
{
    class program
    {
        //delegate declaration
        public delegate string DelegateToDetails(string details);
        public static void Main()
        {
            //Instantiate A delegate
            DelegateToDetails ds = new DelegateToDetails(StudentDetails);
            //Invoke Delegate 
            Console.WriteLine(ds("student name is :: student 1"));
            Console.WriteLine(ds.Invoke("student name is :: student 1"));

        }

        public static string StudentDetails(string name)
        {
            return name;
        }
    }
}
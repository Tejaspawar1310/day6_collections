﻿using Collections_List.Exceptions;
using Collections_List.Model;
using Collections_List.Repository;

ProductRepository productrepsitory = new ProductRepository();
List<Product>productList=productrepsitory.GetAllProducts();

foreach(Product item in productList)
{
    Console.WriteLine(item);
}

//Addproduct to the List
Console.WriteLine("Product List After Addition of Item");
//create product Object
Product product = new Product() { Id=3,Name="laptop",Price=55000};
try
{
    Console.WriteLine(productrepsitory.AddProduct(product));
foreach(Product item in productList)
{
    Console.WriteLine(item);
}

}
catch (ProductAlreadyExistsException ex)
{
    Console.WriteLine(ex.Message);
}

//Delete a product
Console.WriteLine("Delete operation");
Console.WriteLine("enter the product to be deleted");

var itemTobeDeleted = Console.ReadLine();
Console.WriteLine(productrepsitory.DeleteProductByName(itemTobeDeleted));

foreach(var item in productList)
{
    Console.WriteLine(item);
}
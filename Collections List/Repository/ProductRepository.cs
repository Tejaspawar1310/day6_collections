﻿using Collections_List.Exceptions;
using Collections_List.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections_List.Repository
{
    internal class ProductRepository
    {
    //Declare a list
    List<Product> products;
        //Create Constructor here shortcut--> ctor+2tabs
        public ProductRepository()
        {
            //eg:
            //this.Name = name
            products = new List<Product>()
            {
                new Product(){ Id = 1, Name = "TV",Price=20000},
                new Product(){Id=2, Name = "LG",Price=300000}
            };
        }
        //GetAllProducts
        public List<Product> GetAllProducts()
        {
            return products;
        }
        //Add Product
        public string AddProduct(Product product)
        {
            //Check whether the products exists in the List or Not
            var productExists=GetProductByName(product.Name);
            if (productExists == null)
            {
                products.Add(product);
                return $"Product Added Successfuly";
            }
            else
            {
                throw new ProductAlreadyExistsException($"{product.Name} already Exists...");
            };
        }

        private Product GetProductByName(string name)
        {
            return products.Find(p => p.Name == name);
            
        }

        //delete product
        public bool DeleteProductByName(string name)
        {
            var product = GetProductByName(name);
            return product != null?products.Remove(product):false;
        }

        //create A function to Update the Product here 
    }
}
